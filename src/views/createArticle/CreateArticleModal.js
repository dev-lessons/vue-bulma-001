import FormHorizontalField from '../../components/FormHorizontalField.js';

export default {
    name: "CreateArticleModal",
    components: { FormHorizontalField },
    props: {
        showModal: { type: Boolean, required: true }
    },
    emits: [ "update:showModal", "success" ],
    data() {
        return {
            id: null,
            categoryId: null,
            title: null,
            categories: [
                { id: "vue3", title: "Vue 3" },
                { id: "java", title: "Java & SpringFramework" },
                { id: "css", title: "CSS Frameworks" },
                { id: "rust", title: "Rust Language" }
            ]
        }
    },
    setup() {
        const focusedInput = Vue.ref(null);
    },
    created() {
        window.addEventListener('keydown', (e) => {
            if (this.showModal && e.key == 'Escape') {
                this.closeModal();
            }
        });
    },
    watch: {
        showModal(newValue) {
            if (newValue) {
                Vue.nextTick(() => { this.$refs.focusedInput.focus(); });
            }
        }
    },
    methods: {
        closeModal() {
            this.id = null;
            this.title = null;
            this.categoryId = null;
            this.$emit('update:showModal', false);
        },

        okModal() {
            if (this.validateModal()) {
                this.id = 100500;
                this.$emit('success', {
                    id: this.id,
                    title: this.title,
                    categoryId: this.categoryId
                });
                this.closeModal();
            }
        },

        validateTitle() {
            return this.title;
        },

        validateCategory() {
            return this.categoryId;
        },

        validateModal() {
            return this.validateTitle() && this.validateCategory();
        }
    },
    template: `
<div class="modal" :class=" { 'is-active': showModal == true } ">
  <div @click="closeModal()" class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Add new Article</p>
      <button @click="closeModal()" class="delete" aria-label="close"></button>
    </header>
    <section class="modal-card-body">
        <form-horizontal-field label="Title" fieldId="modalArticleTitle">
            <div class="field">
                <p class="control is-expanded">
                    <input
                        ref="focusedInput"
                        id="modalArticleTitle"
                        v-model="title"
                        class="input"
                        type="text"
                        placeholder="Article title"
                        :class="{ 'is-danger': !validateTitle() }"
                        @keyup.enter="okModal()" >
                </p>
            </div>
        </form-horizontal-field>
        <form-horizontal-field label="Category" fieldId="modalArticleCategory">
            <div class="field">
                <div class="select" :class="{ 'is-danger': !validateCategory() }">
                  <select id="modalArticleCategory" v-model="categoryId">
                    <option v-for="category in categories" :value="category.id">
                        {{ category.title }}
                    </option>
                  </select>
                </div>
            </div>
        </form-horizontal-field>
    </section>
    <footer class="modal-card-foot">
      <button @click="okModal()" class="button is-black" :disabled="!validateModal()">Create</button>
      <button @click="closeModal()" class="button">Close</button>
    </footer>
  </div>
</div>
    `
}
