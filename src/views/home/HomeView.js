import CategoryMenuPanel from './CategoryMenuPanel.js';
import ArticlePanel from './ArticlePanel.js';
import ArticleListPanel from './ArticleListPanel.js';

import HtmlPagePanel from '../../components/HtmlPagePanel.js';

export default {
    name: "HomeView",
    components: {
        CategoryMenuPanel, ArticlePanel, ArticleListPanel, HtmlPagePanel
    },
    data() {
        return {
            categorySelected: null,
            articleSelected: null,
        }
    },
    mounted() {
        this.categorySelected = this.$route.params.categoryId;
        this.articleSelected = this.$route.params.articleId;
    },
    created() {
        this.$watch(
            () => this.$route.params, (toParams, previousParams) => {
                // react to route changes...
                this.categorySelected = toParams.categoryId;
                this.articleSelected = toParams.articleId;
            }
        );
    },
    template: `
        <div class="tile is-ancestor">
            <div class="tile is-3">
                <category-menu-panel v-model:selected="categorySelected"></category-menu-panel>
            </div>
            <div class="tile">
                <div class="content">
                    <html-page-panel page="aboutMe" v-if="!categorySelected"></html-page-panel>
                    <article-list-panel :category="categorySelected" v-if="categorySelected && !articleSelected"></article-list-panel>
                    <div v-if="articleSelected">
                        <router-link :to="'/category/' + categorySelected" class="button is-dark mb-5">
                            &leftarrow; Back
                        </router-link>
                        <article-panel :article="articleSelected"></article-panel>
                    </div>
                </div>
            </div>
        </div>
    `
}
