export default {
    name: "CategoryMenuPanel",
    props: [ "selected" ],
    emits: [ "update:selected" ],
    data() {
        return {
            categories: [
                { id: "vue3", title: "Vue 3" },
                { id: "java", title: "Java & SpringFramework" },
                { id: "css", title: "CSS Frameworks" },
                { id: "rust", title: "Rust Language" }
            ]
        }
    },
    methods: {
        selectItem(itemId) {
            this.$emit('update:selected', itemId);
        }
    },
    template: `
        <aside class="menu mb-6">
            <p class="menu-label">General</p>
            <ul class="menu-list">
                <li>
                    <router-link to="/" :class="{ 'is-active': !selected }">About me</router-link>
                </li>
            </ul>

            <p class="menu-label">Articles Category</p>
            <ul class="menu-list">
                <li v-for="category in categories">
                    <router-link :to="'/category/' + category.id" :class="{ 'is-active': selected == category.id }">
                        {{ category.title }}
                    </router-link>
                </li>
            </ul>
        </aside>
    `
}
