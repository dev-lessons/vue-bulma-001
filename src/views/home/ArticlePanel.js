export default {
    name: "ArticlePanel",
    props: [ "article" ],
    data() {
        return {
            content: null
        }
    },
    mounted() {
        this.getArticle();
    },
    methods: {
        async getArticle() {
            try {
                let headerResponse = await fetch("https://baconipsum.com/api/?type=all-meat&format=text&sentences=1");
                let header = await headerResponse.text();
                let response = await fetch("https://baconipsum.com/api/?paras=5&type=all-meat&start-with-lorem=0&format=html");
                let body = await response.text();
                this.content = `<h1 class='title is-capitalized'>${header.replace(".", "")}</h1>${body}`;
            } catch (error) {
                console.log(error);
                this.content = error;
            }
        }
    },
    template: `
        <div v-if="!content">
            <h3 class="title is-5">Article is loading...</h3>
        </div>
        <div v-else v-html="content"></div>
    `
}
