export default {
    name: "ArticleListPanel",
    props: [ "category" ],
    data() {
        return {
            articles: [],
            mapping: {
                "vue3": 0,
                "java": 25,
                "css": 50,
                "rust": 75
            }
        }
    },
    mounted() {
        this.getArticles();
    },
    watch: {
        category: {
            handler(newValue) {
                this.getArticles();
            }
        }
    },
    methods: {
        async getArticles() {
            try {
                let response = await fetch("https://jsonplaceholder.typicode.com/posts");
                this.articles = await response.json();
            } catch (error) {
                console.log(error);
                this.articles = [];
            }
        }
    },
    template: `
        <div>
            <div v-for="(article, index) in articles">
                <div class="card mb-5" v-show="index >= mapping[category] && index < mapping[category] + 25">
                  <div class="card-content">
                    <h4 class="title is-4">{{ article.title }}</h4>
                    <div class="content">{{article.body}}</div>
                    <router-link :to="'/category/' + category + '/article/' + article.id">Read Article</router-link>
                  </div>
                </div>
            </div>
        </div>
    `
}
