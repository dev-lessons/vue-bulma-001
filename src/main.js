import routes from './routes.js';
import NavigationPanel from './components/NavigationalPanel.js';

const app = Vue.createApp({
    components: {
        NavigationPanel
    }
});

app.use(routes.createRouter());

const mountedApp = app.mount("#app");
