export default {
    name: "FormHorizontalField",
    props: [ "label", "fieldId" ],
    template: `
        <div class="field is-horizontal">
            <div class="field-label is-normal">
                <label class="label" :for="fieldId">{{ label }}</label>
            </div>
            <div class="field-body">
                <slot />
            </div>
        </div>
    `
}
