import CreateArticleModal from '../views/createArticle/CreateArticleModal.js';

export default {
    name: "NavigationPanel",
    components: {
        CreateArticleModal
    },
    data() {
        return {
            showNavbar: false,
            createArticleModal: false
        }
    },
    methods: {
        articleCreated(article) {
            console.log("NavigationalPanel.articleCreated(): " + JSON.stringify(article));
            this.$router.push(`/category/${article.categoryId}/article/${article.id}`);
        }
    },
    template: `
        <create-article-modal
            v-model:showModal="createArticleModal"
            @success="articleCreated($event);">
        </create-article-modal>

        <nav :class="{ 'is-active': !showNavbar }" class="navbar is-dark" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <p class="navbar-item has-text-weight-bold">
                    | Vue + Bulma |
                </p>
                <a @click="showNavbar = !showNavbar" :class="{ 'is-active': showNavbar }" role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </a>
            </div>
            <div :class="{ 'is-active': showNavbar }" class="navbar-menu">
                <div class="navbar-start">
                    <router-link to="/" class="navbar-item">Home</router-link>
                </div>
                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            <a @click="createArticleModal = true" class="button is-white">Add Article</a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    `
}
