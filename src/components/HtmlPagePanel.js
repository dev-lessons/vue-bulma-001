export default {
    name: "HtmlPagePanel",
    props: [ "page" ],
    data() {
        return { content: null, error: null }
    },
    mounted() {
        this.getUrlContent(this.page);
    },
    methods: {
        async getUrlContent(page) {
            try {
                let response = await fetch("pages/" + page + ".html");
                this.content = await response.text();
            } catch (error) {
                console.log(error);
                this.error = error;
            }
        },
    },
    template: `
        <div v-html="content || error"></div>
    `
}
