import HomeView from './views/home/HomeView.js';

export default  {
    createRouter() {
        const routes = [
            { path: '/', component: HomeView },
            { path: '/category/:categoryId', component: HomeView },
            { path: '/category/:categoryId/article/:articleId', component: HomeView }
        ];

        return VueRouter.createRouter({
            history: VueRouter.createWebHashHistory(),
            routes,
            scrollBehavior(to, from, savedPosition) {
                // always scroll to top
                return { top: 0 }
            },
        });
    }
}
